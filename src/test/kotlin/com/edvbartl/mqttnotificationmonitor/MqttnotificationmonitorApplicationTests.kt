package com.edvbartl.mqttnotificationmonitor

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.integration.core.MessageProducer


@RunWith(SpringRunner::class)
@SpringBootTest
class MqttnotificationmonitorApplicationTests {

	@MockBean
	private val inbound: MessageProducer? = null

	@Test
	fun contextLoads() {
	}

}
