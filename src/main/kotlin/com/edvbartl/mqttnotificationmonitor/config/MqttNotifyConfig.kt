package com.edvbartl.mqttnotificationmonitor.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@ConfigurationProperties
@Component
class MqttNotifyConfig {
    val mqttserver : String = "localhost"
}
