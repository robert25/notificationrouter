package com.edvbartl.mqttnotificationmonitor.mqtt

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.string
import com.edvbartl.mqttnotificationmonitor.crud.NotificationCrud
import com.edvbartl.mqttnotificationmonitor.data.Notification
import com.twilio.Twilio
import com.twilio.http.TwilioRestClient
import com.twilio.rest.api.v2010.account.Call
import com.twilio.type.PhoneNumber
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.messaging.Message
import org.springframework.messaging.MessageHandler
import org.springframework.stereotype.Component
import java.net.URI

@Component
@ConfigurationProperties
class MqttMessageHandler(private val notificationCrud : NotificationCrud) : MessageHandler {
    val log = LoggerFactory.getLogger(this.javaClass)

    var twilioaccount : String = "undefined"
    var twilioauth : String = "undefined"
    var twilionumber : String = "undefined"
    var defaultrecipient : String = "undefined"
    var callbackurl : String = "undefined"


    @ServiceActivator(inputChannel = "mqttInputChannel")
    override fun handleMessage(message: Message<*>) {
        System.out.println(message.getPayload())
        // save notification
        val notification = Notification(jsonData = java.lang.String.valueOf(message.payload))
        // test for valid json data
        val parser: Parser = Parser()
        try {
            val json: JsonObject = parser.parse(StringBuilder(notification.jsonData)) as JsonObject

            val id = notificationCrud.save(notification)

            var recipient: String? = defaultrecipient

            if (json.containsKey("CONTACTPAGER")) {
                recipient = json.string("CONTACTPAGER")
            }
            log.info("trying to create a call for notification: " + notification.id)

            Twilio.init(twilioaccount, twilioauth)
            val client = TwilioRestClient.Builder(twilioaccount, twilioauth).build()

            val call = Call.creator(PhoneNumber(recipient), PhoneNumber(twilionumber),
                    URI(callbackurl + notification.id
                    )).create()

            log.info("created a call with status:" + call.status)
            log.info("call SID: " + call.getSid())

        } catch (e: Exception) {
            log.info("Error - did not receive valid JSON or failure while trying to create Call",e)
            return
        }

    }
}
