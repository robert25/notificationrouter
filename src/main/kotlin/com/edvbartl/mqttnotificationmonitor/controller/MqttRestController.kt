package com.edvbartl.mqttnotificationmonitor.controller

import com.edvbartl.mqttnotificationmonitor.crud.NotificationCrud
import com.edvbartl.mqttnotificationmonitor.data.Notification
import com.github.kittinunf.fuel.Fuel
import com.twilio.Twilio
import com.twilio.http.TwilioRestClient
import com.twilio.rest.api.v2010.account.Call
import com.twilio.type.PhoneNumber
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.net.URL
import java.util.concurrent.atomic.AtomicLong


@RestController
@RequestMapping("/rest/mqtt")
@ConfigurationProperties
class MqttRestController(private val notificationCrud : NotificationCrud) {

    val log = LoggerFactory.getLogger(this.javaClass)

    var twilioaccount : String = "undefined"
    var twilioauth : String = "undefined"
    var twilionumber : String = "undefined"
    var defaultrecipient : String = "undefined"
    var callbackurl : String = "undefined"


    @RequestMapping(value = "/get/{id}", method = arrayOf(RequestMethod.GET))
    @ResponseBody
    fun get(@PathVariable("id") id: Long): ResponseEntity<String> {
        var notification = notificationCrud.findOne(id)
        if (notification!=null) {
            return ResponseEntity(notification.jsonData,HttpStatus.OK)
        }
        return ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/testalert/{message}", method = arrayOf(RequestMethod.GET))
    @ResponseBody
    fun testAlert(@PathVariable("message") id: String): ResponseEntity<String> {
        val notification = Notification(jsonData = java.lang.String.valueOf("""{
            "WHAT" : "HOST",
            "HOSTNAME" : "xx",
            "HOSTSTATE" : "oo",
            "HOSTOUTPUT" : "${id}"
            }
            """))
        val id = notificationCrud.save(notification)

        if (notification!=null) {
            log.info("created test alert notification:" + notification)
            log.info("created with id : " + notification.id)
            val FROMNUMBER = twilionumber
            val TWILIOAUTH = twilioauth
            val TWILIOACCOUNT = twilioaccount

            val rcpt = defaultrecipient

            Twilio.init(TWILIOACCOUNT, TWILIOAUTH)
            val client = TwilioRestClient.Builder(TWILIOACCOUNT, TWILIOAUTH).build()

            val call = Call.creator(PhoneNumber(rcpt), PhoneNumber(FROMNUMBER),
                    URI(callbackurl + notification.id
                    )).create()

            log.info("status:" + call.status)
            log.info(call.getSid())

            return ResponseEntity(notification.jsonData,HttpStatus.OK)
        }


//        $req->content("From=$sendnumber&To=$recnumber&Url=https%3A%2F%2Fcheck.edv-bartl.at%2Ftwilio%2F$r.xml");

        return ResponseEntity(HttpStatus.NOT_FOUND);
    }
}

