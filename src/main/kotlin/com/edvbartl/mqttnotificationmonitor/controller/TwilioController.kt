package com.edvbartl.mqttnotificationmonitor.controller

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.string
import com.edvbartl.mqttnotificationmonitor.crud.NotificationCrud
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/twilio")
class TwilioController(private val notificationCrud : NotificationCrud) {


    @RequestMapping(value = "/{id}", method = arrayOf(RequestMethod.GET, RequestMethod.POST), produces = arrayOf("application/xml"))
    @ResponseBody
    fun get(@PathVariable("id") id: Long): ResponseEntity<String> {
        var notification = notificationCrud.findOne(id)
        if (notification!=null) {
            // construct message
            val parser: Parser = Parser()
            val json: JsonObject = parser.parse(StringBuilder(notification.jsonData)) as JsonObject

            var message = if (json.string("WHAT")=="HOST")  """<?xml version="1.0" encoding="UTF-8"?>
<Response>
<Say voice="man" language="en">Host ${json.string("HOSTNAME")}
State ${json.string("HOSTSTATE")}
${json.string("HOSTOUTPUT")}
</Say>
</Response>"""
            else
                """<?xml version="1.0" encoding="UTF-8"?>
<Response>
<Say voice="man" language="en">Host ${json.string("HOSTNAME")}
Service ${json.string("SERVICEDESC")} ${json.string("SERVICEOUTPUT")}
</Say>
</Response>"""
            return ResponseEntity(message, HttpStatus.OK)
        }

        return ResponseEntity(HttpStatus.NOT_FOUND)
    }

    fun parse(name: String) : Any? {
        val cls = Parser::class.java
        return cls.getResourceAsStream(name)?.let { inputStream ->
            return Parser().parse(inputStream)
        }
    }
}