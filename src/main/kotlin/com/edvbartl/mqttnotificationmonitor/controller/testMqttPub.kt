package com.edvbartl.mqttnotificationmonitor.controller

import com.edvbartl.mqttnotificationmonitor.config.MqttNotifyConfig
import com.edvbartl.mqttnotificationmonitor.crud.NotificationCrud
import java.util.concurrent.atomic.AtomicLong
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.sun.jmx.remote.util.EnvHelp.getCause
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/rest/pub")
class tradeRestController(private val mqttNotifyConfig : MqttNotifyConfig) {
    private val template = "Hello, %s!"
    private val counter = AtomicLong()




    @RequestMapping(method = arrayOf(RequestMethod.GET))
    @ResponseBody
    fun sayHello(): String {
        val topic = "monitor/notifications/"
        val content = "Message from MqttPublishSample"
        val qos = 2
        val broker = "tcp://localhost:1883"
        val clientId = "JavaSample"
        val persistence = MemoryPersistence()

        try {
            val sampleClient = MqttClient(broker, clientId, persistence)
            val connOpts = MqttConnectOptions()
            connOpts.setCleanSession(true)
            println("Connecting to broker: " + broker)
            sampleClient.connect(connOpts)
            println("Connected")
            println("Publishing message: " + content)
            val message = MqttMessage(content.toByteArray())
            message.setQos(qos)
            sampleClient.publish(topic, message)
            println("Message published")
            sampleClient.disconnect()
            println("Disconnected")
        } catch (me: MqttException) {
            System.out.println("reason " + me.getReasonCode())
            System.out.println("msg " + me.message)
            System.out.println("loc " + me.getLocalizedMessage())
            System.out.println("cause " + me.cause)
            println("excep " + me)
            me.printStackTrace()
        }

        return "OK"
    }
}