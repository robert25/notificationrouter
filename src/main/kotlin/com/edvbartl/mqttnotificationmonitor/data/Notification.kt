package com.edvbartl.mqttnotificationmonitor.data

import javax.persistence.Entity
import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import java.util.Date

@Entity
data class Notification(
        @GeneratedValue(strategy= GenerationType.IDENTITY)
        @Id
        var id: Long? = null,
        var datetime: Date = Date(),
        @Column(length=20000)
        var jsonData: String = ""
)
