package com.edvbartl.mqttnotificationmonitor.crud

import com.edvbartl.mqttnotificationmonitor.data.Notification
import org.springframework.data.repository.CrudRepository


interface NotificationCrud : CrudRepository<Notification, Long> {
}