package com.edvbartl.mqttnotificationmonitor.service

import com.edvbartl.mqttnotificationmonitor.crud.NotificationCrud
import org.springframework.stereotype.Component
import org.springframework.scheduling.annotation.Scheduled



@Component
class CleanupJob(private val notificationCrud : NotificationCrud) {
    @Scheduled(fixedDelay = 60 * 60 * 1000)
    fun cleanupNotifications() {
        notificationCrud.deleteAll()
    }
}