package com.edvbartl.mqttnotificationmonitor

import com.edvbartl.mqttnotificationmonitor.config.MqttNotifyConfig
import com.edvbartl.mqttnotificationmonitor.mqtt.MqttMessageHandler
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter
import org.springframework.integration.core.MessageProducer
import org.springframework.integration.channel.DirectChannel
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.MessageHandler


@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationProperties
class MqttnotificationmonitorApplication {

    val log = LoggerFactory.getLogger(this.javaClass)

    var mqttserver : String = "localhost"

    @Bean
    open fun mqttInputChannel(): MessageChannel {
        System.out.println("channel")
        return DirectChannel()
    }

    @Bean
    open fun inbound(): MessageProducer {
        log.info("connecting to mqtt server: " + mqttserver)
        val factory = DefaultMqttPahoClientFactory()
        factory.setCleanSession(false)
        val adapter = MqttPahoMessageDrivenChannelAdapter("tcp://${mqttserver}:1883", "notificationrouter",
                factory,"monitor/notifications/")
        adapter.setCompletionTimeout(5000)
        adapter.setConverter(DefaultPahoMessageConverter())
        adapter.setQos(1)

        adapter.outputChannel = mqttInputChannel()
        return adapter
    }


}

fun main(args: Array<String>) {
    SpringApplication.run(MqttnotificationmonitorApplication::class.java, *args)
}
