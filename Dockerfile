FROM library/openjdk:8-jdk

ENV mqttserver localhost
ENV twilioauth undefined
ENV twilioaccount undefined
ENV twilionumber undefined
ENV defaultrecipient undefined
ENV callbackurl undefined


ADD build/libs/notificationrouter.jar /opt/notificationrouter.jar

CMD java -jar /opt/notificationrouter.jar
