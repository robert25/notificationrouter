#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

register_notification_parameters("mqtt.pl",
    Dictionary(
        optional_keys = [],
        elements = [
            ( "MQTTSERVER",
              TextAscii(
                title = _("MQTT Server"),
                help = _("MQTT Server reachable from all check_mk nodes"),
                default_value = "localhost"
              ),
            ),
            ( "MQTTTOPIC",
              TextAscii(
                title = _("MQTT Topic"),
                help = _("MQTT Topic like (monitor/notifications/)"),
                default_value = "monitor/notifications/"
              ),
            ),
        ])
    )
