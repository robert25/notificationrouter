#!/usr/bin/env perl
# MQTT
# Bulk: no

########################################################################
# INCLUDES #############################################################
use v5.10;
use Sys::Syslog;
use Data::Dumper;
use Getopt::Long 'HelpMessage';
use JSON;
########################################################################
# END INCLUDES #########################################################

# parameters
GetOptions('help' => sub {HelpMessage(1)}) or HelpMessage(1);
HelpMessage(1) if $ARGV[0];

# Just in case of problems, let's not hang check_mk
$SIG{'ALRM'} = sub {
	say "Timed out";
	exit(2);
};


sub mqtt_send {
    my ($cnx, $rcpt, $user, $msg, $rcpt_is_room, $debug) = @_;
    # $cnx, $rcpt, $p{PARAMETER_USER}, $message, $rcpt_is_room, $debug

    my @args = ("mosquitto_pub","-t","monitor/notifications/","-m",$msg,"-q","2");
    system(@args) == 0
      or warn "mqtt sending failed";
}

sub main {
    # Get all NOTIFY_ Variables from Environment and store it in Hash %p
    my %p = map {$_ =~ /^NOTIFY_(.*)/ ? ($1 => $ENV{$_}) : ()} keys %ENV;


    # Set defaults:
    my $debug = $p{PARAMETER_DEBUG} || undef;
    warn "D-main: mqtt notification handler start, got the following env_vars:\n". Dumper(\%p) if $debug;

    my $timeout = $p{PARAMETER_TIMEOUT} || 3;

    # Set alarm to kill the script.
    warn "D-main: setting Timout for the notification handler - " . $timeout . "s" if $debug;
    alarm($timeout);

    # Further defaults:
    my $url_prefix = $p{PARAMETER_URL_PREFIX} || undef;
    my $resource = $p{PARAMETER_RESOURCE} || $ENV{OMD_SITE};
    my $rcpt_is_room = $p{PARAMETER_CHATROOM} || undef;

    my $json = JSON->new;
    my $message = $json->pretty->encode(\%p);

    warn "D-main: constructed the following message to be sent:\n " . $message if $debug;

    # print Dumper(%p);
    # send message to recipient or chatroom
    mqtt_send ($cnx, $rcpt, $p{PARAMETER_USER}, $message, $rcpt_is_room, $debug);

    exit(0);
}
main();
exit(0);


########################################################################
# POD ##################################################################

=head1 NAME

mqtt.pl - sends notifications via mqtt

=head1 SYNOPSIS
    This is a notificationscript for check_mk monitoring software
    it sends notifications via mqtt

    All Notification environment parameters will be send via MQTT.

    /^NOTIFY_.*/ (Notification Context Parameters from cmc),
    /^NOTIFY_MQTTSERVER.*/ (Contact Context, e.g. CustomAttribut) and
    /^NOTIFY_MQTTTOPIC.*/ (User supplied Parameters via WATO). A WATO File
    for configuration is present so you don't have to care about the parameters below.

    PARAMETER_MQTTSERVER       IP Address or Name of the MQTT Server
    PARAMETER_USER             Username used to connect (unused)
    PARAMETER_PASSWORD         Password used to connect (unused)
    PARAMETER_MQTTTOPIC        MQTT Topic
    PARAMETER_DEBUG            prints additional debug messages on STDERR which gets redirected to
                               ~/var/log/notify.log

    This parameter may be usfull to test the script from cli:

    --help,-h,-?    prints this helpmessage


=head1 VERSION

1.0

=head1 AUTHOR

Robert Bartl - robert@edv-bartl.at

=head1 LICENSE

GPLv2

=cut
